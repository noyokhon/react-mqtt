installing: 
    npm init -y
    npm install mqtt
    npm start 

test:
    npm install mqtt -g
    mqtt pub -t 'com' -h 'test.mosquitto.org' -m 'Your message here!'
